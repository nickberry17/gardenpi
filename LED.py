import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
powerpin = 11
GPIO.setup(powerpin, GPIO.OUT)
GPIO.output(powerpin,False)


speed = 2

def Blink(speed):
    for i in range(5):
        GPIO.output(powerpin, True)
        time.sleep(speed)
        GPIO.output(powerpin, False)
        time.sleep(speed)
        print ("Done")
    GPIO.cleanup()
    print ("Done Overall")

def lightON():
	GPIO.output(powerpin, True)

def lightOFF():
	GPIO.output(powerpin, False)


#Blink(speed)
