# GardenPi

GardenPi was a project for my university coursework.  GardenPi monitors soil moisture and local atmospheric conditions, makes the data available on a local web server, and notifies via email, as well as audio/visual cues whether the garden requires watering.

It includes utilization of GNU/Linux core utilities, developing a class-based Python API and business logic which interfaces with hardware sensors that were installed onto a Raspberry Pi single board computer.  


## Setup

Install the required hardware to your Pi:

- DHT11 Air Temperature and Humidity Sensor
- Soil moisture sensor
- LEDs
- Speaker

Clone this repo, and edit it to match the pins you are using.

If you wish to serve a webpage, install nginx, or your preferred webserver and host the directory where the HTML file is written to.

Demo
[GardenPi demo video](https://www.youtube.com/watch?v=-_JHUUiurzk)