# Emailer.py class acecsses the SensorData text file, and sends the latest value to the 
# specified email address.

import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def sendEmail():
	sensorData = []

	data = open("SensorData.txt", "r")

	for line in data:
		sensorData.append(line)
	 
	fromaddr = "emailAddress"
	toaddr = "emailAddress"
	msg = MIMEMultipart()
	msg['From'] = fromaddr
	msg['To'] = toaddr
	msg['Subject'] = "GardenPi Alert"

	body = "Your garden needs watering.  The current water level is only: %s" % (str(sensorData[-1]))  

	msg.attach(MIMEText(body, 'plain'))

	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login(fromaddr, "PASSWORD")
	text = msg.as_string()
	server.sendmail(fromaddr, toaddr, text)
	server.quit()