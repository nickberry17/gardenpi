import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
powerpinGreen = 11
powerpinRed = 32

GPIO.setup(powerpinGreen, GPIO.OUT)
GPIO.output(powerpinGreen,False)

GPIO.setup(powerpinRed, GPIO.OUT)
GPIO.output(powerpinRed,False)

speed = 2

def Cleanup():
	GPIO.cleanup()

def Blink(speed, pin):
    for i in range(5):
        GPIO.output(pin, True)
        time.sleep(speed)
        GPIO.output(pin, False)
        time.sleep(speed)
        #print ("Blinking ", pin)
    GPIO.cleanup()
    #print ("Done blinking.")

def greenLightON():
	GPIO.output(powerpinGreen, True)
	Cleanup()

def greenLightOFF():
	GPIO.output(powerpinGreen, False)
	Cleanup()

def redLightON():
	GPIO.output(powerpinRed, True)
	Cleanup()

def redLightOFF():
	GPIO.output(powerpinRed, False)
	Cleanup()



#Blink(speed)
